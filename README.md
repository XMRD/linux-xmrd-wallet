# XMRD Linux Wallet

## install packages
* please open terminal and got to the folder that downloaded from gitlab repository.
    ```bash
    cd /path-to-downloads
    ./install.sh
    ```
* extract tar file and run wallet
    ```bash
    tar -xvf monerodollar-gui-x86_64-ubuntu18.04.tar.xz
    cd bin
    ./start-gui.sh
    ```

